<%@ page language="java" import="java.util.*" pageEncoding="gb2312"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="gb2312">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>查询签到信息</title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui-1.9.2.custom.css" type="text/css">
	<script src="${pageContext.request.contextPath}/public/media/js/jquery-1.10.1.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/jquery-ui-1.9.2.custom.js" type = "text/javascript" language="javascript"></script>
	<script src="${pageContext.request.contextPath}/js/share.js" type = "text/javascript"></script>
    <script>
    	function deleteSigninActivity(id){
				var isDelete = confirm("您确认要删除吗？");
				if(isDelete){
					//要删除
					location.href = "";
				}
			}
		$(function(){
				$("#findButton").click(function(){
					var start = $("#startDate").val();
					$("#startDate")[0].value = start.split("/").join("-");
					var end = $("#endtDate").val();
					if(end){
						$("#endtDate")[0].value = end.split("/").join("-");
					}
						$("#findForm").submit();
				});
				
			});
			
    </script>
    <style type="text/css">
    	.ui-datepicker-time {
    		display:inline-block;
    		height:33px;
    	}
    </style>
  </head>
  <body>
    <div class="container">
	     <form id="findForm" action="signinActivityAction_pageQuery.action" class="form-inline" method="post">
	      <div class="form-group">
	      	<label for="date">日期范围:</label>
			<input id="date" type="text" class="ui-datepicker-time" readonly value=""/>
			<div class="ui-datepicker-css">	
			    <div class="ui-datepicker-quick">
			        <p>快捷日期<a class="ui-close-date">X</a></p>
			        <div>
			            <input class="ui-date-quick-button" type="button" value="今天" alt="0"  name=""/>
			            <input class="ui-date-quick-button" type="button" value="昨天" alt="-1" name=""/>
			            <input class="ui-date-quick-button" type="button" value="7天内" alt="-6" name=""/>
			            <input class="ui-date-quick-button" type="button" value="14天内" alt="-13" name=""/>
			            <input class="ui-date-quick-button" type="button" value="30天内" alt="-29" name=""/>
			            <input class="ui-date-quick-button" type="button" value="60天内" alt="-59" name=""/>
			        </div>
			    </div>
			    <div class="ui-datepicker-choose">
			        <p>自选日期</p>
			        <div class="ui-datepicker-date">
			            <input name="startDate" id="startDate" class="startDate" name="startDate" readonly type="text">
			           -
			            <input name="endDate" id="endDate" class="endDate" name="endDate" readonly  type="text" disabled onchange="datePickers()">
			        
			        </div>
			    </div>
			</div>
			 </div>
	          <div class="form-group">
	              <input type="reset"  class="btn btn-primary"/>
	          </div>
	          <div class="form-group">
	              <input id="findButton" type="button"  class="btn btn-primary" value="查询"/>
	          </div>
	    </form>
    <hr>
    <table class="table table-bordered">
	      <thead>
		        <tr>
		          <th>发起日期</th>
		          <th>发起人</th>
		          <th>签到班级</th>
		          <th>应签到人数</th>
		          <th>实签到人数</th>
		        </tr>
	      </thead>
	       <tbody>
		      	<c:forEach var="signinActivity" items="${pageBean.rows}">
		      		 <tr>
			      		  <td>${signinActivity.activityDate}</td>
				          <td>${signinActivity.user.name}</td>
				          <td>
				          <c:forEach varStatus="varStatus" var="majorClass" items="${signinActivity.majorClasses}" >
				              ${majorClass.name}<c:if test="${!varStatus.last}">,</c:if>
				          </c:forEach>
				          </td>
				          <td>
				          	  ${signinActivity.totalPeople}
				          </td>
				          <td>
				         	  ${fn:length(signinActivity.signinDetails)}
				          </td>
				          <td>
				          	    <a  class="btn btn-danger btn-xs" onclick="deleteSigninActivity('${signinActivity.id}')">删除</a>
				          	    <a  class="btn btn-success btn-xs" href="">补签</a>
				          </td>
				     </tr>
		      	</c:forEach>
      </tbody>
     </table>
</div>

<div style="width: 500px; margin: 0 auto; margin-top: 50px;">
        <ul class="pagination pagination-sm" style="text-align: center; margin-top: 10px;">
            <!-- 上一页 -->
            <!-- 判断当前页是否是第一页 -->
            <c:if test="${pageBean.currentPage==1 }">
                <li class="disabled">
                    <a href="javascript:void(0);" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
            </c:if>
            <c:if test="${pageBean.currentPage!=1 }">
                <li>
                    <a href="${pageContext.request.contextPath }/admin/manage/searcharticlelist?currentPage=${pageBean.currentPage-1}" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
            </c:if>
            <c:forEach begin="1" end="${pageBean.totalPage }" var="page">
                <!-- 判断当前页 -->
                <c:if test="${pageBean.currentPage==page }">
                    <li class="active"><a href="javascript:void(0);">${page}</a></li>
                </c:if>
                <c:if test="${pageBean.currentPage!=page }">
                    <li><a href="${pageContext.request.contextPath }/admin/manage/searcharticlelist?currentPage=${page}">${page}</a></li>
                </c:if>

            </c:forEach>

            <!-- 判断当前页是否是最后一页 -->
            <c:if test="${pageBean.currentPage==pageBean.totalPage }">
                <li class="disabled">
                    <a href="javascript:void(0);" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </c:if>
            <c:if test="${pageBean.currentPage!=pageBean.totalPage }">
                <li>
                    <a href="${pageContext.request.contextPath }/admin/manage/searcharticlelist?currentPage=${pageBean.currentPage+1}" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </c:if>

        </ul>
    </div>
    </div>
  </body>
</html>