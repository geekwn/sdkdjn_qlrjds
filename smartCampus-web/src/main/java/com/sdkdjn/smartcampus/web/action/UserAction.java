package com.sdkdjn.smartcampus.web.action;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.sdkdjn.smartcampus.entity.User;
import com.sdkdjn.smartcampus.service.IUserService;
import com.sdkdjn.smartcampus.utils.MD5Utils;
import com.sdkdjn.smartcampus.web.action.base.BaseAction;

@Controller
@Scope("prototype")
public class UserAction extends BaseAction<User> {

	private static final long serialVersionUID = 1L;
	@Autowired
	private IUserService userService;
	// 验证码
	private String checkcode;

	public void setCheckcode(String checkcode) {
		this.checkcode = checkcode;
	}

	public String login() {
		// 从Session中获取生成的验证码
		String validatecode = (String) ServletActionContext.getRequest().getSession().getAttribute("key");
		// 校验验证码是否输入正确
		if (StringUtils.isNotBlank(checkcode) && checkcode.equals(validatecode)) {
			// 使用shiro框架提供的方式进行认证操作
			Subject subject = SecurityUtils.getSubject();// 获得当前用户对象,状态为“未认证”
			AuthenticationToken token = new UsernamePasswordToken(model.getId(), MD5Utils.md5(model.getPassword()));// 创建用户名密码令牌对象
			try {
				subject.login(token);
			} catch (Exception e) {
				e.printStackTrace();
				return LOGIN;
			}
			User user = (User) subject.getPrincipal();
			ServletActionContext.getRequest().getSession().setAttribute("loginUser", user);
			return HOME;
		} else {
			// 输入的验证码错误,设置提示信息，跳转到登录页面
			this.addActionError("验证码输入错误！");
			return LOGIN;
		}
	}

	public String welcome() {
		return "welcome";
	}

}
