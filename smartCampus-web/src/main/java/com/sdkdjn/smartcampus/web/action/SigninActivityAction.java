package com.sdkdjn.smartcampus.web.action;

import java.sql.Timestamp;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.struts2.ServletActionContext;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.sdkdjn.smartcampus.dao.ISigninActivityDao;
import com.sdkdjn.smartcampus.entity.SigninActivity;
import com.sdkdjn.smartcampus.web.action.base.BaseAction;

@Controller
@Scope("prototype")
public class SigninActivityAction extends BaseAction<SigninActivity> {

	private static final long serialVersionUID = 1L;
	@Autowired
	private ISigninActivityDao signinActivityDao;
	private String startDate;
	private String endDate;

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@RequiresPermissions("signinactivity_list") // 执行这个方法，需要当前用户具有signinactivity_list这个权限
	public String pageQuery() {

		DetachedCriteria detachedCriteria = pageBean.getDetachedCriteria();
		detachedCriteria = DetachedCriteria.forClass(SigninActivity.class);
		if (startDate != null) {
			Timestamp startTime = new Timestamp(System.currentTimeMillis());
			Timestamp endTime = new Timestamp(System.currentTimeMillis());
			if (endDate != null) {
				startDate += " 00:00:00"; // 开始的一天的开始时间字符串
				endDate += " 23:59:59"; // 结束的一天的结束时间字符串
				try {
					startTime = Timestamp.valueOf(startDate);
					endTime = Timestamp.valueOf(endDate);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				startDate += " 00:00:00";
				try {
					startTime = Timestamp.valueOf(startDate);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			detachedCriteria.add(Restrictions.between("activityDate", startTime, endTime));
		}
		signinActivityDao.pageQuery(pageBean);
		ServletActionContext.getRequest().setAttribute("signinActivity", pageBean);
		return LIST;
	}
}
