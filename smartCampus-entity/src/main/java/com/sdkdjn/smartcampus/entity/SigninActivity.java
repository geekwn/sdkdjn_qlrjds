package com.sdkdjn.smartcampus.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

public class SigninActivity implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private String id;
	private User user;
	private Integer activityType;
	private String information;
	private Timestamp activityDate;
	private Integer totalPeople;
	private Set<MajorClass> majorClasses = new HashSet<MajorClass>(0);
	private Set<SigninDetail> signinDetails = new HashSet<SigninDetail>(0);

	public SigninActivity() {
	}

	public SigninActivity(String id, User user, Integer activityType, Timestamp activityDate, Integer totalPeople) {
		this.id = id;
		this.user = user;
		this.activityType = activityType;
		this.activityDate = activityDate;
		this.totalPeople = totalPeople;
	}

	public SigninActivity(String id, User user, Integer activityType, String information, Timestamp activityDate,
			Integer totalPeople, Set<MajorClass> majorClasses, Set<SigninDetail> signinDetails) {
		this.id = id;
		this.user = user;
		this.activityType = activityType;
		this.information = information;
		this.activityDate = activityDate;
		this.totalPeople = totalPeople;
		this.majorClasses = majorClasses;
		this.signinDetails = signinDetails;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getActivityType() {
		return this.activityType;
	}

	public void setActivityType(Integer activityType) {
		this.activityType = activityType;
	}

	public String getInformation() {
		return this.information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public Timestamp getActivityDate() {
		return this.activityDate;
	}

	public void setActivityDate(Timestamp activityDate) {
		this.activityDate = activityDate;
	}

	public Integer getTotalPeople() {
		return this.totalPeople;
	}

	public void setTotalPeople(Integer totalPeople) {
		this.totalPeople = totalPeople;
	}

	public Set<MajorClass> getMajorClasses() {
		return majorClasses;
	}

	public void setMajorClasses(Set<MajorClass> majorClasses) {
		this.majorClasses = majorClasses;
	}

	public Set<SigninDetail> getSigninDetails() {
		return signinDetails;
	}

	public void setSigninDetails(Set<SigninDetail> signinDetails) {
		this.signinDetails = signinDetails;
	}

}