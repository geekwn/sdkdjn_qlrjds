package com.sdkdjn.smartcampus.entity;

import java.sql.Timestamp;

public class SigninDetail implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private String id;
	private SigninActivity signinActivity;
	private Student student;
	private Timestamp signinDate;

	public SigninDetail() {
	}

	public SigninDetail(String id, SigninActivity signinActivity, Student student, Timestamp signinDate) {
		this.id = id;
		this.signinActivity = signinActivity;
		this.student = student;
		this.signinDate = signinDate;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public SigninActivity getSigninActivity() {
		return this.signinActivity;
	}

	public void setSigninActivity(SigninActivity signinActivity) {
		this.signinActivity = signinActivity;
	}

	public Student getStudent() {
		return this.student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Timestamp getSigninDate() {
		return this.signinDate;
	}

	public void setSigninDate(Timestamp signinDate) {
		this.signinDate = signinDate;
	}

}