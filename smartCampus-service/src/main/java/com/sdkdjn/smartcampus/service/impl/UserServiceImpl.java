package com.sdkdjn.smartcampus.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sdkdjn.smartcampus.dao.IUserDao;
import com.sdkdjn.smartcampus.entity.User;
import com.sdkdjn.smartcampus.service.IUserService;
import com.sdkdjn.smartcampus.utils.MD5Utils;

@Service
@Transactional
public class UserServiceImpl implements IUserService {
	@Autowired
	private IUserDao userDao;

	public User login(User user) {
		// 使用MD5加密密码
		String password = MD5Utils.md5(user.getPassword());
		return userDao.findUserByIdAndPassword(user.getId(), password);
	}
}
