CREATE DATABASE `smart_campus`;
USE `smart_campus`;
CREATE TABLE smart_campus_school(
	`id` VARCHAR(20) PRIMARY KEY,
	`name` VARCHAR(50) NOT NULL,
	`information` VARCHAR(255)
);
CREATE TABLE smart_campus_user(
	`id` VARCHAR(20) PRIMARY KEY,
	`school_id` VARCHAR(10) NOT NULL,
	`name` VARCHAR(50) NOT NULL,
	`telephone` VARCHAR(20) NOT NULL,
	`password` VARCHAR(32) NOT NULL,
	`openid` VARCHAR(50),
	CONSTRAINT smart_campus_user_school_id FOREIGN KEY (school_id) REFERENCES smart_campus_school(id)
);
CREATE TABLE smart_campus_major_class(
	`id` VARCHAR(20) PRIMARY KEY,
	`school_id` VARCHAR(20) NOT NULL,
	`name` VARCHAR(20) NOT NULL,
	`information` VARCHAR(255),
	CONSTRAINT smart_campus_major_class_school_id FOREIGN KEY (school_id) REFERENCES smart_campus_school(id)
);
CREATE TABLE smart_campus_student(
	`id` VARCHAR(20) PRIMARY KEY,
	`school_id` VARCHAR(10) NOT NULL,
	`major_class_id` VARCHAR(20) NOT NULL,
	`name` VARCHAR(10) NOT NULL,
	`password` VARCHAR(32) NOT NULL,
	`openid` VARCHAR(50),
	CONSTRAINT smart_campus_student_school_id FOREIGN KEY (school_id) REFERENCES smart_campus_school(id),
	CONSTRAINT smart_campus_student_major_class_id FOREIGN KEY (major_class_id) REFERENCES smart_campus_major_class(id)
);
CREATE TABLE smart_campus_repair_order(
	`id` VARCHAR(32) PRIMARY KEY,
	`school_id` VARCHAR(20) NOT NULL,
	`order_date` TIMESTAMP NOT NULL,
	`student_id` VARCHAR(10) NOT NULL,
	`picture_url` VARCHAR(10),
	`place` VARCHAR(100) NOT NULL,
	`information` VARCHAR(255),
	`order_state` INT DEFAULT 0,
	`rating` INT,
	`evaluation_content` VARCHAR(255),
	CONSTRAINT smart_campus_repair_order_school_id FOREIGN KEY (school_id) REFERENCES smart_campus_school(id),
	CONSTRAINT smart_campus_repair_order_student_id FOREIGN KEY (student_id) REFERENCES smart_campus_student(id)
);
CREATE TABLE smart_campus_signin_activity(
	`id` VARCHAR(32) PRIMARY KEY,
	`user_id` VARCHAR(20) NOT NULL, 
	`activity_type` INT NOT NULL,
	`information` VARCHAR(255),
	`activity_date` TIMESTAMP NOT NULL,
	`total_people` INT NOT NULL,
	CONSTRAINT smart_campus_sign_activity_user_id FOREIGN KEY (user_id) REFERENCES smart_campus_user(id)
);
CREATE TABLE smart_campus_classactivity_relationship(
	`signin_activity_id` VARCHAR(32) NOT NULL,
	`major_class_id` VARCHAR(20) NOT NULL,
	CONSTRAINT smart_campus_classactivity_relationship_signin_activity_id FOREIGN KEY (signin_activity_id) REFERENCES smart_campus_signin_activity(id),
	CONSTRAINT smart_campus_classactivity_relationship_major_class_id FOREIGN KEY (major_class_id) REFERENCES smart_campus_major_class(id)
);
CREATE TABLE smart_campus_signin_detail(
	id VARCHAR(32) PRIMARY KEY,
	`signin_activity_id` VARCHAR(32) NOT NULL,
	`student_id` VARCHAR(20) NOT NULL,
	`signin_date` TIMESTAMP NOT NULL,
	CONSTRAINT t_signin_details_activity_id FOREIGN KEY (signin_activity_id) REFERENCES smart_campus_signin_activity(id),
	CONSTRAINT t_signin_details_student_id FOREIGN KEY (student_id) REFERENCES smart_campus_student(id)
);
CREATE TABLE smart_campus_auth_role(
	`id` VARCHAR(32) PRIMARY KEY,
	`name` VARCHAR(10) NOT NULL,
	`code` VARCHAR(50) NOT NULL, 
	`information` VARCHAR(255)
);
CREATE TABLE smart_campus_user_role(
	`auth_role_id` VARCHAR(32) NOT NULL,
	`user_id` VARCHAR(20) NOT NULL,
	CONSTRAINT smart_campus_auth_role_auth_role_id FOREIGN KEY (auth_role_id) REFERENCES smart_campus_auth_role(id),
	CONSTRAINT smart_campus_auth_role_user_id FOREIGN KEY (user_id) REFERENCES smart_campus_user(id)
);
CREATE TABLE smart_campus_auth_function(
	`id` VARCHAR(32) PRIMARY KEY,
	`name` VARCHAR(50) NOT NULL,
	`code` VARCHAR(50) NOT NULL,
	`page` VARCHAR(255) NOT NULL
);
CREATE TABLE smart_campus_role_function(
	`auth_function_id` VARCHAR(32) NOT NULL,
	`auth_role_id` VARCHAR(32) NOT NULL,
	CONSTRAINT smart_campus_role_function_auth_function_id FOREIGN KEY (auth_function_id) REFERENCES smart_campus_auth_function(id),
	CONSTRAINT smart_campus_role_function_auth_role_id FOREIGN KEY (auth_role_id) REFERENCES smart_campus_auth_role(id)
);