package com.sdkdjn.smartcampus.dao.impl;

import org.springframework.stereotype.Repository;

import com.sdkdjn.smartcampus.dao.IMajorClassDao;
import com.sdkdjn.smartcampus.dao.base.impl.BaseDaoImpl;
import com.sdkdjn.smartcampus.entity.MajorClass;

@Repository
public class MajorClassDaoImpl extends BaseDaoImpl<MajorClass> implements IMajorClassDao {

}
