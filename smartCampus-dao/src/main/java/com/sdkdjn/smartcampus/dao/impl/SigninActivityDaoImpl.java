package com.sdkdjn.smartcampus.dao.impl;

import org.springframework.stereotype.Repository;

import com.sdkdjn.smartcampus.dao.ISigninActivityDao;
import com.sdkdjn.smartcampus.dao.base.impl.BaseDaoImpl;
import com.sdkdjn.smartcampus.entity.SigninActivity;

@Repository
public class SigninActivityDaoImpl extends BaseDaoImpl<SigninActivity> implements ISigninActivityDao {

}
