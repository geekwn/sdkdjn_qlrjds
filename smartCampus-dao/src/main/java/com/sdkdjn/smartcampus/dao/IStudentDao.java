package com.sdkdjn.smartcampus.dao;

import com.sdkdjn.smartcampus.dao.base.IBaseDao;
import com.sdkdjn.smartcampus.entity.Student;

public interface IStudentDao extends IBaseDao<Student> {

}
