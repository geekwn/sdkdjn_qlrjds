package com.sdkdjn.smartcampus.dao;

import com.sdkdjn.smartcampus.dao.base.IBaseDao;
import com.sdkdjn.smartcampus.entity.MajorClass;

public interface IMajorClassDao extends IBaseDao<MajorClass> {

}
