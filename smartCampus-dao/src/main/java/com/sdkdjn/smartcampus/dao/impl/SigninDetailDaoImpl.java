package com.sdkdjn.smartcampus.dao.impl;

import org.springframework.stereotype.Repository;

import com.sdkdjn.smartcampus.dao.ISigninDetailDao;
import com.sdkdjn.smartcampus.dao.base.impl.BaseDaoImpl;
import com.sdkdjn.smartcampus.entity.SigninDetail;

@Repository
public class SigninDetailDaoImpl extends BaseDaoImpl<SigninDetail> implements ISigninDetailDao {

}
