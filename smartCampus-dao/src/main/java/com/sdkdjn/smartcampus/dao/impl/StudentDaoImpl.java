package com.sdkdjn.smartcampus.dao.impl;

import org.springframework.stereotype.Repository;

import com.sdkdjn.smartcampus.dao.IStudentDao;
import com.sdkdjn.smartcampus.dao.base.impl.BaseDaoImpl;
import com.sdkdjn.smartcampus.entity.Student;

@Repository
public class StudentDaoImpl extends BaseDaoImpl<Student> implements IStudentDao {

}
