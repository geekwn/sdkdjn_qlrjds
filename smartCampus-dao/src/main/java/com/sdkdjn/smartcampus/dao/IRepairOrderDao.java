package com.sdkdjn.smartcampus.dao;

import com.sdkdjn.smartcampus.dao.base.IBaseDao;
import com.sdkdjn.smartcampus.entity.RepairOrder;

public interface IRepairOrderDao extends IBaseDao<RepairOrder> {

}
