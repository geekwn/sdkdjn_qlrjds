package com.sdkdjn.smartcampus.dao.impl;

import org.springframework.stereotype.Repository;

import com.sdkdjn.smartcampus.dao.IRepairOrderDao;
import com.sdkdjn.smartcampus.dao.base.impl.BaseDaoImpl;
import com.sdkdjn.smartcampus.entity.RepairOrder;

@Repository
public class RepairOrderDaoImpl extends BaseDaoImpl<RepairOrder> implements IRepairOrderDao {

}
