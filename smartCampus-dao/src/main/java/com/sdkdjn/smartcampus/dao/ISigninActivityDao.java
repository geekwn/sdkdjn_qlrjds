package com.sdkdjn.smartcampus.dao;

import com.sdkdjn.smartcampus.dao.base.IBaseDao;
import com.sdkdjn.smartcampus.entity.SigninActivity;

public interface ISigninActivityDao extends IBaseDao<SigninActivity> {

}
